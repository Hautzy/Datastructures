#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef int DATA;

typedef struct _NODE{
	DATA data;
	struct _NODE* next;
}NODE;

typedef struct _STACK{
	int cnt;
	NODE* start;
}STACK;

void push(STACK* stack, DATA data);
DATA pop(STACK* stack);
DATA peek(STACK* stack);

int isempty(STACK* stack);

void iterate(STACK* stack, void(*fnc)(DATA data));

void print(DATA data);
