#include "func.h"

void main(){
	STACK stack = {0};
	
	push(&stack, 0);
	push(&stack, 1);
	push(&stack, 2);
	push(&stack, 3);
	push(&stack, 4);
	push(&stack, 5);
	
	iterate(&stack, print);
	printf("|--->%d\n", pop(&stack));
	iterate(&stack, print);
	printf("|--->%d\n", pop(&stack));
	iterate(&stack, print);
	printf("|--->%d\n", pop(&stack));
	iterate(&stack, print);
	printf("|--->%d\n", pop(&stack));
	iterate(&stack, print);
	printf("|--->%d\n", pop(&stack));
	iterate(&stack, print);
	printf("|--->%d\n", pop(&stack));
	iterate(&stack, print);
	printf("|--->%d\n", pop(&stack));
	iterate(&stack, print);
}
