#include "func.h"

void push(STACK* stack, DATA data){
	if(stack == NULL)	return;
	NODE* add = calloc(1, sizeof(NODE));
	add->data = data;
	add->next = stack->start;
	stack->start = add;
	stack->cnt++;
}
DATA pop(STACK* stack){
	if(stack == NULL)	return;
	if(!isempty(stack)){
		NODE* help = stack->start;
		stack->start = help->next;
		DATA data = help->data;
		free(help);
		stack->cnt--;
		return data;
	}else{
		return -999;
	}
}
DATA peek(STACK* stack){
	if(stack == NULL)	return;
	if(!isempty(stack)){
		return stack->start->data;
	}else{
		return -999;
	}
}

int isempty(STACK* stack){
	if(stack == NULL)	return 1;
	else if(stack->start == NULL)	return 1;
	else	return 0;
}

void iterate(STACK* stack, void(*fnc)(DATA data)){
	if(stack == NULL)	return;
	NODE* cur = stack->start;
	int i;
	for(i = 0;i < stack->cnt;i++){
		(*fnc)(cur->data);
		cur = cur->next;
	}
}

void print(DATA data){
	printf(" %d ", data);
}
