#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define STRLEN 256

typedef struct _STUDENT{
	char fname[STRLEN];
	char lname[STRLEN];
	int prio;
}STUDENT;
typedef struct _NODE{
	STUDENT* data;
	int prio;
	struct _NODE* next;
}NODE;
typedef struct _PQUEUE{
	int cnt;
	NODE* head;
	NODE* tail;
}PQUEUE;

typedef STUDENT DATA;

//PQUEUE
void enqueue(PQUEUE* queue, DATA* data);
DATA* dequeue(PQUEUE* queue);
int isempty(PQUEUE* queue);
void iterate(PQUEUE* queue, void(*fnc)(DATA* data));

void print(DATA* data);
//STUDENTS
STUDENT* cstudent(char str[]);
//STRINGS
void normstr(char str[]);
void clearstr(char str[]);
void spltstr(char str[], char part[], int ind, char sep);



