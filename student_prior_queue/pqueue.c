#include "pqueue.h"

void enqueue(PQUEUE* queue, DATA* data){
	if(queue == NULL)	return;
	NODE* add = calloc(1, sizeof(NODE));
	add->data = data;
	add->prio = data->prio;
	if(!isempty(queue)){
		NODE* cur = queue->head;
		NODE* last = NULL;
		int i;
		for(i = 0;i < queue->cnt;i++){
			if(cur->prio > add->prio){
				add->next = cur;
				if(last == NULL){
					queue->head = add;
				}else{
					last->next = add;
				}
				queue->cnt++;
				return;
			}
			last = cur;
			cur = cur->next;
		}
		queue->tail->next = add;
		queue->tail = queue->tail->next;
	}else{
		queue->head = add;
		queue->tail = add;
	}
	queue->cnt++;
}
DATA* dequeue(PQUEUE* queue){
	if(queue == NULL)	return;
	if(!isempty(queue)){
		NODE* del = queue->head;
		DATA* data = del->data;
		queue->head = queue->head->next;
		queue->cnt--;
		if(del == queue->tail){
			queue->tail = NULL;
		}
		free(del);
		return data;
	}else{
		return NULL;
	}
}

int isempty(PQUEUE* queue){
	if(queue == NULL)	return 1;
	else if(queue->head == NULL)	return 1;
	else return 0;
}

void iterate(PQUEUE* queue, void(*fnc)(DATA* data)){
	if(queue == NULL)	return;
	NODE* cur = queue->head;
	int i;
	for(i = 0;i < queue->cnt;i++){
		(*fnc)(cur->data);
		cur = cur->next;
	}
}

void print(DATA* data){
	printf(" %d] %s %s \n", data->prio, data->fname, data->lname);
}

STUDENT* cstudent(char str[]){
	STUDENT* st = calloc(1, sizeof(STUDENT));
	normstr(str);
	char help[STRLEN] = {0};
	spltstr(str, help, 3, ';');
	st->prio = atoi(help);
	spltstr(str, help, 4, ';');
	strcpy(st->fname, help);
	spltstr(str, help, 5, ';');
	strcpy(st->lname, help);
	return st;
}

void normstr(char str[]){
	int i = 0;
	while(str[i] && i < STRLEN && str[i++] != '\n');
	str[i - 1] = 0;
	str[STRLEN - 1] = 0;
}
void clearstr(char str[]){
	int i = 0;
	while(str[i] != 0 && i < STRLEN){
		str[i] = 0;
		i++;
	}
}
void spltstr(char str[], char part[], int ind, char sep){
	int i = 0;
	int j = 0;
	int cnt = 0;
	clearstr(part);
	for(;i < strlen(str);i++){
		if(str[i] == sep){
			cnt++;
			if(cnt > ind)	return;
		}else if(cnt == ind){
			part[j++] = str[i];
		}
	}
}
