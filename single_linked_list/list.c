#include "func.h"

void main(){
	LIST list = {0};
	addhead(&list, 3);
	addtail(&list, 4);
	addtail(&list, 5);
	addhead(&list, 1);
	addhead(&list, 0);
	add(&list, 2, 2);

	printf("\n");
	iterate(&list, print);

	del(&list, 0);

	printf("\n");
	iterate(&list, print);

	del(&list, 2);

	printf("\n");
	iterate(&list, print);
}
