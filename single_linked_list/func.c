#include "func.h"

void addhead(LIST* list, DATA data){
	if(list == NULL)	return;
	NODE* add = calloc(1, sizeof(NODE));
	add->data = data;
	add->next = list->start;
	list->start = add;
	list->cnt++;
}
void addtail(LIST* list, DATA data){
	if(list == NULL)	return;
	if(list->cnt != 0){
		NODE* add = calloc(1, sizeof(NODE));
		add->data = data;
		NODE* cur = list->start;
		for(;cur->next != NULL;cur = cur->next);
		cur->next = add;
		list->cnt++;
	}else{
		addhead(list, data);
	}
}
void add(LIST* list, DATA data, int ind){
	if(list == NULL || ind < 0 || (ind >= list->cnt && ind != 0))	return;
	NODE* add = calloc(1, sizeof(NODE));
	add->data = data;
	NODE* cur = list->start;
	NODE* last = NULL;
	for(;ind--;cur = cur->next){
		last = cur;
	}
	add->next = cur;
	if(last == NULL){
		addhead(list, data);
		return;
	}else{
		last->next = add;
	}
	list->cnt++;
}

DATA del(LIST* list, int ind){
	if(list == NULL || ind < 0 || ind >= list->cnt)	return;
	NODE* cur = list->start;
	NODE* last = NULL;
	for(;ind--;cur = cur->next){
		last = cur;
	}
	if(last == NULL){
		list->start = cur->next;
	}else{
		last->next = cur->next;
	}
	list->cnt--;
}

void iterate(LIST* list, void(*fnc)(DATA data)){
	if(list == NULL)	return;
	NODE* cur = list->start;
	int i = 0;
	for(;i < list->cnt;i++){
		(*fnc)(cur->data);
		cur = cur->next;
	}
}

void print(DATA data){
	printf(" %d ", data);
}
