#include "func.h"

void enqueue(QUEUE* queue, DATA data){
	if(queue == NULL)	return;
	NODE* add = calloc(1, sizeof(NODE));
	add->data = data;
	if(isempty(queue)){
		queue->head = add;
		queue->tail = add;
	}else{
		queue->tail->next = add;
		queue->tail = queue->tail->next;
	}
	queue->cnt++;
}
DATA dequeue(QUEUE* queue){
	if(queue == NULL)	return;
	if(!isempty(queue)){
		NODE* cur = queue->head;
		DATA data = cur->data;
		queue->head = queue->head->next;
		if(cur == queue->tail){
			queue->tail = NULL;
		}
		free(cur);
		queue->cnt--;
		return data;
	}else{
		return -999;
	}
}

int isempty(QUEUE* queue){
	if(queue == NULL)	return 1;
	else if(queue->head == NULL)	return 1;
	else	return 0;
}

void iterate(QUEUE* queue, void(*fnc)(DATA data)){
	if(queue == NULL)	return;
	NODE* cur = queue->head;
	int i;
	for(i = 0;i < queue->cnt;i++){
		(*fnc)(cur->data);
		cur = cur->next;
	}
}

void print(DATA data){
	printf(" %d ", data);
}
