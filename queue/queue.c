#include "func.h"

void main(){
	QUEUE queue = {0};
	enqueue(&queue, 0);
	enqueue(&queue, 1);
	enqueue(&queue, 2);
	enqueue(&queue, 3);
	enqueue(&queue, 4);
	enqueue(&queue, 5);
	
	iterate(&queue, print);
	
	dequeue(&queue);
	printf("\n");
	iterate(&queue, print);
	dequeue(&queue);
	printf("\n");
	iterate(&queue, print);
	dequeue(&queue);
	printf("\n");
	iterate(&queue, print);
	dequeue(&queue);
	printf("\n");
	iterate(&queue, print);
	dequeue(&queue);
	printf("\n");
	iterate(&queue, print);
	dequeue(&queue);
	printf("\n");
	iterate(&queue, print);
	printf("\n%d", dequeue(&queue));
}
