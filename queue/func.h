#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef int DATA;

typedef struct _NODE{
	DATA data;
	struct _NODE* next;
}NODE;

typedef struct _QUEUE{
	int cnt;
	NODE* head;
	NODE* tail;
}QUEUE;

void enqueue(QUEUE* queue, DATA data);
DATA dequeue(QUEUE* queue);

int isempty(QUEUE* queue);

void iterate(QUEUE* queue, void(*fnc)(DATA data));

void print(DATA data);
