#include "func.h"

void addhead(LIST* list, DATA data){
	if(list == NULL)	return;
	NODE* add = calloc(1, sizeof(NODE));
	add->data = data;
	if(list->cnt != 0){
		add->next = list->start;
		add->prev = list->start->prev;
		add->next->prev = add;
		add->prev->next = add;
	}else{
		add->next = add;
		add->prev = add;
	}
	list->start = add;
	list->cnt++;
}
void addtail(LIST* list, DATA data){
	if(list == NULL)	return;
	if(list->cnt != 0){
		NODE* add = calloc(1, sizeof(NODE));
		add->data = data;
		add->next = list->start;
		add->prev = list->start->prev;
		add->next->prev = add;
		add->prev->next = add;
		list->cnt++;
	}else{
		addhead(list, data);
	}
}
void add(LIST* list, DATA data, int ind){
	if(list == NULL || ind < 0 || (ind >= list->cnt && ind != 0))	return;
	if(ind != 0){
		NODE* add = calloc(1, sizeof(NODE));
		add->data = data;
		NODE* cur = list->start;
		for(;ind--;cur = cur->next);
		add->next = cur;
		add->prev = cur->prev;
		add->next->prev = add;
		add->prev->next = add;
		list->cnt++;
	}else{
		addhead(list, data);
	}
}

void del(LIST* list, int ind){
	if(list == NULL || ind < 0 || ind >= list->cnt)	return;
	NODE* cur = list->start;
	if(list->cnt == 0){
		list->start = NULL;
		return;
	}
	if(ind != 0){
		for(;ind--;cur = cur->next);
		cur->next->prev = cur->prev;
		cur->prev->next = cur->next;
	}else{
		list->start->next->prev = list->start->prev;
		list->start->prev->next = list->start->next;
		list->start = list->start->next;
	}
	list->cnt--;
}

void iterate_forward(LIST* list, void(*fnc)(DATA data)){
	if(list == NULL)	return;
	NODE* cur = list->start;
	int i;
	for(i = 0;i < list->cnt;i++){
		(*fnc)(cur->data);
		cur = cur->next;
	}
}
void iterate_backward(LIST* list, void(*fnc)(DATA data)){
	if(list == NULL)	return;
	NODE* cur = list->start->prev;
	int i;
	for(i = 0;i < list->cnt;i++){
		(*fnc)(cur->data);
		cur = cur->prev;
	}
}

void print(DATA data){
	printf(" %d ", data);
}
