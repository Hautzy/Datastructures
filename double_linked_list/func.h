#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef int DATA;

typedef struct _NODE{
	DATA data;
	struct _NODE* next;
	struct _NODE* prev;
}NODE;

typedef struct _LIST{
	int cnt;
	NODE* start;
}LIST;

void addhead(LIST* list, DATA data);
void addtail(LIST* list, DATA data);
void add(LIST* list, DATA data, int ind);

void del(LIST* list, int ind);

void iterate_forward(LIST* list, void(*fnc)(DATA data));
void iterate_backward(LIST* list, void(*fnc)(DATA data));

void print(DATA data);
