#include "func.h"

void main(){
	PQUEUE queue = {0};
	
	enqueue(&queue, 0, 0);
	enqueue(&queue, 1, 1);
	enqueue(&queue, 2, 2);
	enqueue(&queue, 4, 4);
	enqueue(&queue, 5, 5);
	enqueue(&queue, -1, -1);
	enqueue(&queue, 3, 3);
	enqueue(&queue, -2, -1);
	enqueue(&queue, -3, -1);
	
	iterate(&queue, print);
	printf("|-->%d\n", dequeue(&queue));
	iterate(&queue, print);
	printf("|-->%d\n", dequeue(&queue));
	iterate(&queue, print);
	printf("|-->%d\n", dequeue(&queue));
	iterate(&queue, print);
	printf("|-->%d\n", dequeue(&queue));
	iterate(&queue, print);
	printf("|-->%d\n", dequeue(&queue));
	iterate(&queue, print);
	printf("|-->%d\n", dequeue(&queue));
	iterate(&queue, print);
	printf("|-->%d\n", dequeue(&queue));
	iterate(&queue, print);
	printf("|-->%d\n", dequeue(&queue));
	iterate(&queue, print);
	printf("|-->%d\n", dequeue(&queue));
	iterate(&queue, print);
	printf("|-->%d\n", dequeue(&queue));
	iterate(&queue, print);
}
