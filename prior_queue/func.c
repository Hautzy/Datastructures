#include "func.h"

void enqueue(PQUEUE* queue, DATA data, PRIO prio){
	if(queue == NULL)	return;
	NODE* add = calloc(1, sizeof(NODE));
	add->data = data;
	add->prio = prio;
	if(!isempty(queue)){
		NODE* cur = queue->head;
		NODE* last = NULL;
		int i;
		for(i = 0;i < queue->cnt;i++){
			if(cur->prio < add->prio){
				add->next = cur;
				if(last == NULL){
					queue->head = add;
				}else{
					last->next = add;
				}
				queue->cnt++;
				return;
			}
			last = cur;
			cur = cur->next;
		}
		queue->tail->next = add;
		queue->tail = queue->tail->next;
	}else{
		queue->head = add;
		queue->tail = add;
	}
	queue->cnt++;
}
DATA dequeue(PQUEUE* queue){
	if(queue == NULL)	return;
	if(!isempty(queue)){
		NODE* help = queue->head;
		queue->head = help->next;
		if(help == queue->tail){
			queue->tail = NULL;
		}
		DATA data = help->data;
		free(help);
		queue->cnt--;
		return data;
	}else{
		return -999;
	}
}

int isempty(PQUEUE* queue){
	if(queue == NULL)	return 1;
	else if(queue->head == NULL)	return 1;
	else	return 0;
}

void iterate(PQUEUE* queue, void(*fnc)(DATA data)){
	if(queue == NULL)	return;
	NODE* cur = queue->head;
	int i;
	for(i = 0;i < queue->cnt;i++){
		(*fnc)(cur->data);
		cur = cur->next;
	}
}

void print(DATA data){
	printf(" %d ", data);
}
