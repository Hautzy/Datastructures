#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef int DATA;
typedef int PRIO;

typedef struct _NODE{
	struct _NODE* next;
	DATA data;
	PRIO prio;
}NODE;

typedef struct _PQUEUE{
	int cnt;
	NODE* head;
	NODE* tail;
}PQUEUE;

void enqueue(PQUEUE* queue, DATA data, PRIO prio);
DATA dequeue(PQUEUE* queue);

int isempty(PQUEUE* queue);

void iterate(PQUEUE* queue, void(*fnc)(DATA data));

void print(DATA data);
